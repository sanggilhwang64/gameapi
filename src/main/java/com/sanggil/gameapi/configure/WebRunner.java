package com.sanggil.gameapi.configure;

import com.sanggil.gameapi.service.InitDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class WebRunner implements ApplicationRunner {
    private final InitDataService initDataService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        initDataService.setFirstMoney();
        initDataService.setFirstArtifact();
        initDataService.setFirstUseArtifact();
    }
}
