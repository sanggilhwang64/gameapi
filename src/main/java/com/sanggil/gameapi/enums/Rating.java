package com.sanggil.gameapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Rating {
    NORMAL("노말"),
    RARE("레어"),
    EPIC("에픽"),
    LEGEND("전설"),
    GOD("신화")
    ;
    private final String name;
}
