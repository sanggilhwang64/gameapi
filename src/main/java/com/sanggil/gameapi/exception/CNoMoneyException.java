package com.sanggil.gameapi.exception;

public class CNoMoneyException extends RuntimeException {
    public CNoMoneyException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNoMoneyException(String msg) {
        super(msg);
    }

    public CNoMoneyException() {
        super();
    }
}

