package com.sanggil.gameapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
