package com.sanggil.gameapi.entity;

import com.sanggil.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UseArtifact {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "게임유물")
    @JoinColumn(name = "gameArtifactId", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private GameArtifact gameArtifact;

    @ApiModelProperty(notes = "수량")
    @Column(nullable = false)
    private Integer artifactCount;

    public void resetCount() {
        this.artifactCount = 0;
    }

    public void putCountPius() {
        this.artifactCount += 1;
    }

    private UseArtifact(UseArtifactBuilder builder) {
        this.gameArtifact = builder.gameArtifact;
        this.artifactCount = builder.artifactCount;
    }

    public static class UseArtifactBuilder implements CommonModelBuilder<UseArtifact> {
        private final GameArtifact gameArtifact;
        private final Integer artifactCount;

        public UseArtifactBuilder(GameArtifact gameArtifact) {
            this.gameArtifact = gameArtifact;
            this.artifactCount = 0;
        }

        @Override
        public UseArtifact build() {
            return new UseArtifact(this);
        }
    }
}
