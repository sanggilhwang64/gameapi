package com.sanggil.gameapi.repository;

import com.sanggil.gameapi.entity.Money;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MoneyRepository extends JpaRepository<Money, Long> {
}
