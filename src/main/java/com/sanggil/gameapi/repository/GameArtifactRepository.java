package com.sanggil.gameapi.repository;

import com.sanggil.gameapi.entity.GameArtifact;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameArtifactRepository extends JpaRepository<GameArtifact, Long> {
}
