package com.sanggil.gameapi.service;

import com.sanggil.gameapi.entity.GameArtifact;
import com.sanggil.gameapi.entity.Money;
import com.sanggil.gameapi.entity.UseArtifact;
import com.sanggil.gameapi.enums.Rating;
import com.sanggil.gameapi.model.GameArtifactCreateRequest;
import com.sanggil.gameapi.repository.GameArtifactRepository;
import com.sanggil.gameapi.repository.MoneyRepository;
import com.sanggil.gameapi.repository.UseArtifactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InitDataService {
    private final MoneyRepository moneyRepository;
    private final GameArtifactRepository gameArtifactRepository;
    private final UseArtifactRepository useArtifactRepository;

    public void setFirstMoney() {
        Optional<Money> originData = moneyRepository.findById(1L);

        if (originData.isEmpty()) {
            Money addData = new Money.MoneyBuilder().build();
            moneyRepository.save(addData);
        }
    }

    public void setFirstArtifact() {
        List<GameArtifact> originList = gameArtifactRepository.findAll();

        if (originList.size() == 0) {
            List<GameArtifactCreateRequest> result = new LinkedList<>();
            GameArtifactCreateRequest result1 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.NORMAL, "가죽 모자", "leather_helmet.jpg", 17.5, 0D).build();
            result.add(result1);
            GameArtifactCreateRequest result2 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.NORMAL, "가죽 갑옷", "leather_armor.jpg", 17.5, 0D).build();
            result.add(result2);
            GameArtifactCreateRequest result3 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.NORMAL, "가죽 신발", "leather_shoes.jpg", 17.5, 0D).build();
            result.add(result3);
            GameArtifactCreateRequest result4 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.NORMAL, "목검", "wood_sword.jpg", 17.5, 0D).build();
            result.add(result4);
            GameArtifactCreateRequest result5 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.RARE, "철 모자", "iron_helmet.jpg", 5D, 15D).build();
            result.add(result5);
            GameArtifactCreateRequest result6 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.RARE, "철 갑옷", "iron_armor.jpg", 5D, 15D).build();
            result.add(result6);
            GameArtifactCreateRequest result7 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.RARE, "철 신발", "iron_shoes.jpg", 5D, 15D).build();
            result.add(result7);
            GameArtifactCreateRequest result8 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.RARE, "철 검", "iron_sword.jpg", 5D, 15D).build();
            result.add(result8);
            GameArtifactCreateRequest result9 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.EPIC, "은 모자", "silver_helmet.jpg", 2D, 8.75).build();
            result.add(result9);
            GameArtifactCreateRequest result10 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.EPIC, "은 갑옷", "silver_armor.jpg", 2D, 8.75).build();
            result.add(result10);
            GameArtifactCreateRequest result11 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.EPIC, "은 신발", "silver_shoes.jpg", 2D, 8.75).build();
            result.add(result11);
            GameArtifactCreateRequest result12 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.EPIC, "은 검", "silver_sword.jpg", 2D, 8.75).build();
            result.add(result12);
            GameArtifactCreateRequest result13 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.LEGEND, "금 모자", "gold_helmet.jpg", 0.5, 1D).build();
            result.add(result13);
            GameArtifactCreateRequest result14 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.LEGEND, "금 갑옷", "gold_armor.jpg", 0.5, 1D).build();
            result.add(result14);
            GameArtifactCreateRequest result15 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.LEGEND, "금 신발", "gold_shoes.jpg", 0.5, 1D).build();
            result.add(result15);
            GameArtifactCreateRequest result16 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.LEGEND, "금 검", "gold_sword.jpg", 0.5, 1D).build();
            result.add(result16);
            GameArtifactCreateRequest result17 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.GOD, "다이아몬드 모자", "diamond_helmet.jpg", 0D, 0.25).build();
            result.add(result17);
            GameArtifactCreateRequest result18 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.GOD, "다이아몬드 갑옷", "diamond_armor.jpg", 0D, 0.25).build();
            result.add(result18);
            GameArtifactCreateRequest result19 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.GOD, "다이아몬드 신발", "diamond_shoes.jpg", 0D, 0.25).build();
            result.add(result19);
            GameArtifactCreateRequest result20 = new GameArtifactCreateRequest.GameArtifactCreateRequestBuilder(Rating.GOD, "다이아몬드 검", "diamond_sword.jpg", 0D, 0.25).build();
            result.add(result20);

            result.forEach(item -> {
                GameArtifact addData = new GameArtifact.GameArtifactBuilder(item).build();
                gameArtifactRepository.save(addData);
            });
        }
    }

    public void setFirstUseArtifact() {
        List<UseArtifact> useArtifacts = useArtifactRepository.findAll();

        if (useArtifacts.size() == 0) {
            List<GameArtifact> gameArtifacts = gameArtifactRepository.findAll();

            gameArtifacts.forEach(item -> {
                UseArtifact addData = new UseArtifact.UseArtifactBuilder(item).build();
                useArtifactRepository.save(addData);
            });
        }
    }

}
