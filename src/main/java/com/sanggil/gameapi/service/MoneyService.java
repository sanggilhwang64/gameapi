package com.sanggil.gameapi.service;

import com.sanggil.gameapi.entity.Money;
import com.sanggil.gameapi.model.MoneyResponse;
import com.sanggil.gameapi.repository.MoneyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MoneyService {
    private final MoneyRepository moneyRepository;

    public MoneyResponse putMoneyPlus() {
        Money money = moneyRepository.findById(1L).orElseThrow(ClassCastException::new);
        money.plusMoney();

        Money result = moneyRepository.save(money);

        return new MoneyResponse.MoneyResponseBuilder(result).build();
    }
}
