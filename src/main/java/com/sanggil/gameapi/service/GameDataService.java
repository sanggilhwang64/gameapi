package com.sanggil.gameapi.service;

import com.sanggil.gameapi.entity.Money;
import com.sanggil.gameapi.entity.UseArtifact;
import com.sanggil.gameapi.exception.CMissingDataException;
import com.sanggil.gameapi.model.FirstConnectDataResponse;
import com.sanggil.gameapi.model.MoneyResponse;
import com.sanggil.gameapi.model.UseArtifactItem;
import com.sanggil.gameapi.repository.MoneyRepository;
import com.sanggil.gameapi.repository.UseArtifactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GameDataService {
    private final MoneyRepository moneyRepository;
    private final UseArtifactRepository useArtifactRepository;

    /**
     * 게임 접속하면 맨 처음 받아올 데이터
     *
     * @return
     */
    public FirstConnectDataResponse getFirstData() {
        FirstConnectDataResponse result = new FirstConnectDataResponse();
        // 결과값을 담을 빈 그릇 생성.. 여기는 빌더 쓰지 않아서 new FirstConnectDataResponse() 빈 그릇 만들수 있다

        Money money = moneyRepository.findById(1L).orElseThrow(CMissingDataException::new); // 돈 원본 데이터 가져오기
        result.setMoneyResponse(new MoneyResponse.MoneyResponseBuilder(money).build()); // 돈 정보 가공해서 넣기
        result.setUseArtifactItems(this.getMyArtifacts()); // 유물 정보 가공해서 넣기

        return result;
    }

    public void gameReset() {
        Money money = moneyRepository.findById(1L).orElseThrow(CMissingDataException::new);
        money.resetMoney();
        moneyRepository.save(money);

        List<UseArtifact> originList = useArtifactRepository.findAll();
        for (UseArtifact item : originList) {
            item.resetCount();
            useArtifactRepository.save(item);
        }
    }

    /**
     * 내가 보유한 유물 컬렉션을 가져온다.
     *
     * @return 보유한 유물 컬렉션
     */
    public List<UseArtifactItem> getMyArtifacts() {
        List<UseArtifact> originList = useArtifactRepository.findAllByIdGreaterThanEqualOrderByIdAsc(1L); // 내가 가지고있는 유물리스트 전체를 불러온다.

        List<UseArtifactItem> result = new LinkedList<>(); // 결과값을 담을 빈 리스트 생성

        originList.forEach(item -> result.add(new UseArtifactItem.UseArtifactItemBuilder(item).build()));

        return result;
    }
}
